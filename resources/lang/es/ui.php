<?php

return [

    'Compra_e_Vendi'=> 'Vende y compra',
    'Nuovo_ed_Usato'=> 'nuevos y usados',
    'Con'=> 'Con',
    'PuoiComprareOnlineInModoSempliceESicuro'=> 'puedes comprar online de forma fácil, rápida y segura',
    'Gratis'=> 'Gratis',
    'ESenzaCommissioni'=> 'y sin comisiones',
    'CosaStaiCercando'=> '¿Qué estás buscando?',
    'Cerca'=> 'Buscar',
    'CreaAnnuncio'=> 'Crear anuncio',
    'LavoraConNoi'=> 'Trabaja con nosotros',
    'ITuoiAnnunci'=> 'Sus anuncios',
    'IlTuoProfilo'=> 'Tu perfil',
    'Presto.it_è_il_punto_di_riferimento_per_i_tuoi_annunci!'=> '¡Presto.it es el punto de referencia para sus anuncios!',

];