<?php

return [

    'Compra_e_Vendi'=> 'Sell ​​and Buy',
    'Nuovo_ed_Usato'=> 'new and used',
    'Con'=> 'With',
    'PuoiComprareOnlineInModoSempliceESicuro'=> 'you can buy online easily, quickly and safely',
    'Gratis'=> 'Free',
    'ESenzaCommissioni'=> 'and commission-free',
    'CosaStaiCercando'=> 'What are you look for?',
    'Cerca'=> 'Search',
    'CreaAnnuncio'=> 'Create ad',
    'LavoraConNoi'=> 'Work with us',
    'ITuoiAnnunci'=> 'Your ads',
    'Presto.it_è_il_punto_di_riferimento_per_i_tuoi_annunci!'=> 'Presto.it is the reference point for your ads!',

];