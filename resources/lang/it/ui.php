<?php

return [

    'Compra_e_Vendi'=> 'Compra e Vendi',
    'Nuovo_ed_Usato'=> 'nuovo ed usato',
    'Con'=> 'Con',
    'PuoiComprareOnlineInModoSempliceESicuro'=> 'puoi comprare online in modo semplice, rapido e sicuro',
    'Gratis'=> 'Gratis',
    'ESenzaCommissioni'=> 'e senza commissioni',
    'CosaStaiCercando'=> 'Cosa stai cercando?',
    'Cerca'=> 'Cerca',
    'CreaAnnuncio'=> 'Crea annuncio',
    'LavoraConNoi'=> 'Lavora con noi',
    'ITuoiAnnunci'=> 'I tuoi annunci',
    'IlTuoProfilo'=> 'Il tuo profilo',
    'ListaUtenti'=> 'Lista utenti registrati',
    'Presto.it_è_il_punto_di_riferimento_per_i_tuoi_annunci!'=> 'Presto.it è il punto di riferimento per i tuoi annunci!',
];