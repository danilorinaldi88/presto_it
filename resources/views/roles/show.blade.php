<x-layouts>
    {{-- form action="{{route('roles.update')}}" method="post">
    @csrf
    @method('PUT')
  
        <h2 value="{{$user->name}}" >{{$user->name}}</h2>

        <h2 value="{{$user->email}}" >{{$user->email}}</h2>
  
      
    <button type="submit" class="btn btn-danger" >Salva</button>
 </form>   --}} 


 <div class="row justify-content-center">
    <div class="col-12 col-lg-4">
        <form method="post" action="{{route('roles.update', compact('user'))}}">
         @csrf
         @method('PUT')
            <div class="form-group mt-2">
                 <label for="name" class="mb-3">Nome e Cognome</label>
                 <input name="name" type="name" class="form-control" id="name" aria-describedby="emailH
                 elp" value="{{ $user->name }}" >
                 @error('name') 
                 <div class="alert alert-danger">{{$message}}  </div>
                 @enderror
            </div>
            <div class="form-group mt-4">
                 <label for="exampleInputEmail1" class="mb-3">Indirizzo email</label>
                 <input type="email" name="email" class="form-control" id="email" aria-describedby="emai
                 lHelp"  value="{{ $user->email }}" >
                 @error('email') 
                 <div class="alert alert-danger">{{$message}}</div>
                 @enderror
            </div>
            <select name="role_id"class="custom-select custom-select-sm mb-3">
                <option selected>Seleziona ruolo</option>
                <option value="1" >Amministratore</option>
                <option value="2" >Revisore</option>
                <option value="0" >Utente</option>
            </select>
            <small> Premendo "modifica dati" verrà spedita automaticamente una mail all'utente per avvisarlo del cambio di ruolo </small> <br>
            <button type="submit" class="btn btn-red mt-3">Modifica dati</button>
             </form>
    </div>
</div>
</x-layouts>