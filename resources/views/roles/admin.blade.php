<x-layouts>
    <div class="container">
        <div class="row ">
            <div class="col-12">
        
                <h1>Utenti Registrati</h1>
                <ul>
                    <div class="mt-5 mb-5">
                        
                    </div>
                    
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">email</th>
                                <th scope="col">ruolo</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                      <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td> @if($user->role_id  == 1)
                            <h6>Amministratore</h6>
                            @elseif ( $user->role_id == 2)
                             <h6>Revisore</h6>
                            @else
                            <h6>Utente</h6>
                            @endif
                        </td>
                        <td> <a href="{{route('roles.show', compact('user'))}}" class="btn btn-secondary btn-sm">Vedi</a> </td>
                      <td> 
                        
                        @if(Auth::user()->name !== $user->name)
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#elimina{{$user->id}}"> Elimina</button></td>
                        @endif
                    </tr>

                    <div class="modal fade" id="elimina{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                             Sei sicuro di voler eliminare questo utente {{$user->name}}
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                              <form action="{{route('profile.delete' , compact('user'))}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" >Elimina</button>
                                
                            </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </tbody>
                  </table>
                </ul>
            </div>
        </div>
    </div>

</x-layouts>