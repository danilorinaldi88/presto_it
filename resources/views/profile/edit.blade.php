<x-layouts>
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 mb-5">
                <hr>
                <h2 class="text-center h-medium l-height-40">Modifica il tuo profilo</strong></h2>
                <hr>
            </div>
        </div>
            
        <div class="row justify-content-center">
            <div class="col-12 col-lg-4">
                <form method="post" action="{{route('profile.update', compact('user'))}}">
                 @csrf
                 @method('PUT')
                    <div class="form-group mt-2">
                         <label for="name" class="mb-3">Nome e Cognome</label>
                         <input name="name" type="name" class="form-control" id="name" aria-describedby="emailH
                         elp" value="{{ Auth::user()->name }}" >
                         @error('name') 
                         <div class="alert alert-danger">{{$message}}  </div>
                         @enderror
                    </div>
                    <div class="form-group mt-4">
                         <label for="exampleInputEmail1" class="mb-3">Indirizzo email</label>
                         <input type="email" name="email" class="form-control" id="email" aria-describedby="emai
                         lHelp"  value="{{ Auth::user()->email }}" >
                         @error('email') 
                         <div class="alert alert-danger">{{$message}}</div>
                         @enderror
                    </div>
                         <button type="submit" class="btn btn-red mt-3">Modifica dati</button>
                     </form>
            </div>
        </div>
     </div>
</x-layouts>