<x-layouts>
    <x-slot name="title">Il tuo profilo | Presto.it</x-slot>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6">
                <hr>
                <h2 class="text-center h-medium l-height-40">Ecco i dati del tuo tuo profilo</h2>
                <h6 class="text-center my-4 l-height-30">Puoi modificare i tuoi dati cliccando sul relativo tasto <em>modifica annuncio</em></h6>
                <hr>
            </div>
        </div>
        <div class="row justify-content-center text-center mt-4">
            <div class="col-12 col-lg-4 profile-edit py-3 px-4">
                <img src="https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png" alt="" class="rounded-circle img-fluid d-block mx-auto h-150">
                <p class="mt-3">
                    Nome e Cognome: <br><strong>{{Auth::user()->name}}</strong>
                <p>
                <p class="mt-2">
                    Indirizzo email: <br><strong>{{Auth::user()->email}}</strong>
                <p>
                <p>
                    <a class="btn btn-red mt-3
                    " href="{{route('profile.edit', compact('user'))}}">Modifica Profilo</a>
                </p>
            </div>
        </div>
    </div>

    
    
</x-layouts>