@switch($classification)
    @case('UNKNOW')

        <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 16%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        
        @break

    @case('VERY_UNLIKELY')

        <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 32%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        
        @break

    @case('UNLIKELY')

        <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        
        @break

    @case('POSSIBLE')

        <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" style="width: 64%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        
        @break

    @case('LIKELY')

        <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" style="width: 16%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        
        @break
    @case('UNKNOWN')

        <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" style="width: 82%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        
        @break

    @case('VERY_LIKELY')

        <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        
        @break
        
    @default

    <div class="progress">
        <div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
        
@endswitch