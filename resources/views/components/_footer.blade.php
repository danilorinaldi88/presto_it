
<!-------------------------FOOTER------------------------>

<footer class="bg-blue">
    <div class="container">
      <div class="row py-5 justify-content-center text-white">
        <div class="col-12 col-md-4 col-lg-4 p-3">
          <a class="navbar-brand mb-3 logo-madv" href="{{ url('/') }}">
            <img class="img-logo-footer" src="\img\logo.png" alt="logo madv">
          </a>
          <p class="text-thin">{{ __('ui.Presto.it_è_il_punto_di_riferimento_per_i_tuoi_annunci!')}}<br>Grazie al nostro portale potrai vendere al miglior prezzo e trovare le migliori occasioni per acquistare i prodotti che tanto desideri.</p>
        </div>
        <div class="col-12 col-md-4 col-lg-4 mt-2 p-3 pl-lg-5">
          <h5 class="text-bold mt-1 mb-4 text-aqua">Aiuto e altre informazioni</h5>
          <ul class="no-padding">
            <li class="menu-sec mt-3 no-bullets text-thin">
              <a class="text-white" href="{{route('contacts.create')}}">Lavora con noi
              </a>
            </li>
            <li class="menu-sec mt-3 no-bullets text-thin">
              <a class="text-white" href="#">Contattaci
              </a>
            </li>
            <li class="menu-sec mt-3 no-bullets text-thin">
              <a class="text-white" href="#">FAQ
              </a>
            </li>
            <li class="menu-sec mt-3 no-bullets text-thin">
              <a class="text-white" href="#">Privacy Policy
              </a>
            </li>
          </ul>
        </div>
        <div class="col-12 col-md-4 col-lg-4 mt-2 p-3 pl-lg-5">
          <h5 class="text-bold mt-1 mb-4 text-aqua">Social</h5>
          <ul class="no-padding">
            <li class="menu-sec d-inline mr-3">
              <i class="fab fa-2x fa-facebook"></i>
            </li>
            <li class="menu-sec d-inline mr-3">
              <i class="fab fa-2x fa-instagram"></i>
            </li>
            <li class="menu-sec d-inline">
              <i class="fab fa-2x fa-twitter"></i>
            </li>
          </ul>
        </div>
      </div>
  </footer>