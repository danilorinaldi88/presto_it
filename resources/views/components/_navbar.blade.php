<nav class="navbar navbar-expand-lg navbar-light bg-blue language-nav">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <form action="{{ route('locale', 'it')}}" method="POST">
                        @csrf
                        <button class="nav-link" type="submit" style='background-color:transparent; border:none;'>
                            <span class="text-light-grey text-s text-thin"> ITA /</span>
                        </button>
                    </form> 
                 </li>
                
                <li class="nav-item">
                    <form action="{{ route('locale', 'en')}}" method="POST">
                        @csrf
                        <button class="nav-link" type="submit" style='background-color:transparent; border:none;'>
                            <span class="text-light-grey text-s text-thin">ENG /</span>
                        </button>
                    </form> 
                </li>
                <li class="nav-item">
                    <form action="{{ route('locale', 'es')}}" method="POST">
                        @csrf
                        <button class="nav-link" type="submit" style='background-color:transparent; border:none;'>
                            <span class="text-light-grey text-s text-thin">ESP</span>
                        </button>
                    </form> 
                </li>   
            </ul>
        </div>
    </div>
</nav>


<nav class="navbar navbar-expand-xl bg-blue shadow-sm navbar-custom">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="\img\logo.png" class="img-logo"alt="">
        </a>
        <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav ml-auto list-margin">
                <li>
                    <a class="nav-link text-white" href="{{route('announcement.new')}}">{{ __('ui.CreaAnnuncio')}}</a>
                </li>                         
                    <li class="nav-item dropdown hidden-md d-lg-none">
                        <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Categorie
                        </a>
                        <div class="dropdown-menu text-blue" aria-labelledby="categoriesDropdown">
                            @foreach ($categories as $category)
                            <a class="dropdown-item nav-link text-blue" href="{{route('public.announcements.category', [
                            $category->name,
                            $category->id,
                            ])}}">{{$category->name}}</a>   
                            @endforeach
                        </div>
                    </li>

                    {{-- Test lavora con noi --}}
                    @if(Auth::user() && Auth::user()->is_revisor)
                    <li>
                    </li>
                    @elseif(Auth::user())
                    <li class="nav-item">
                        <a class="nav-link text-white" href="{{route('contacts.create')}}">{{ __('ui.LavoraConNoi')}}</a>
                    </li>
                    @endif

                    <!-- Link revisore -->
                    @if(Auth::user() && Auth::user()->is_revisor)
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('revisor.home')}}">Da revisionare
                                <span class="badge badge-pill badge-warning"> 
                                {{\App\Models\Announcement::ToBeRevisionedCount()}}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{route('revisor.trash')}}">Cancellati
                                <span class="badge badge-pill badge-danger"> 
                                {{\App\Models\Announcement::TrashedCount()}}
                                </span>
                            </a>
                        </li>

                    @endif
                    <!-- Authentication Links -->
                    @guest
                    @if (Route::has('login'))
                    <li class="nav-item">
                        <a class="nav-link ml-3 text-white btn btn-red" href="{{ route('login') }}">Accedi</a>
                    </li>
                    @endif

                   
                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link ml-2 text-white btn btn-red" href="{{ route('register') }}">Registrati</a>
                    </li>
                    @endif
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                           
                        
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                        <a href="{{route('announcement.list')}}" class="dropdown-item" >
                            {{ __('ui.ITuoiAnnunci')}}
                        </a>
                        <a href="{{route('profile.create')}}" class="dropdown-item" >
                            {{ __('ui.IlTuoProfilo')}}
                        </a>
                        @auth
                        @if(Auth::user()->role_id == 1)
                        <a href="{{route('roles.admin')}}" class="dropdown-item" >
                            {{ __('ui.ListaUtenti')}}
                        </a>
                        @endif
                        @endauth
                        <a class="dropdown-item " href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </div>
                   
                   
                    @endguest
                </li>
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-light bg-light-grey sub-nav">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mx-auto">
                @foreach ($categories as $category)
                    <li class="nav-item">
                        <a class="nav-link sub-menu-link mr-4" href="{{route('public.announcements.category', [
                    $category->name,
                    $category->id,
                    ])}}">{{$category->name}}</a>
                    </li>   
                @endforeach
            </ul>
        </div>
    </div>
</nav>

