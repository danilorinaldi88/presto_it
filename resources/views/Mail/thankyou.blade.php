<x-layouts>
    <x-slot name="title">Grazie per averci inviato la richiesta | Presto.it</x-slot>

    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <hr>
                <h2 class="text-center l-height-50"><strong>{{ Auth::user()->name }}</strong>, grazie per averci inviato la tua richiesta per diventare un nostro revisore.</h2>
                <h6 class="text-center my-4 l-height-30">Ti comunicheremo al più presto la nostra decisione.<br> Speriamo di poterti avere presto nel nostro team!</h6>
                <p class="text-center mb-5">
                    <a href="{{route('home')}}" class="btn btn-red text-center">Torna all'homepage</a>
                </p>
                <hr>
            </div>
        </div>
    </div>
</x-layouts>