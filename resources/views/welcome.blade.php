<x-layouts>
    @if(session('access.denied.revisor.only'))
        <div class="alert alert-danger">
            Accesso non consentito solo per revisori!
        </div>
    @endif
    <header class="masthead bg-white py-md-0 mb-sm-5">
        <div class="container h-100 bg-white">
            <div class="row h-100 align-items-center">
                <div class="col-lg-6 text-left">
                    <h1 class="text-blue masthead-title display-5">{{ __('ui.Compra_e_Vendi')}}<br>{{ __('ui.Nuovo_ed_Usato')}}.<br><span class="text-underline">{{ __('ui.Gratis')}}</span> {{ __('ui.ESenzaCommissioni')}}.</h1>
                    <p class="text-blue text-thin my-4">{{ __('ui.Con')}} <strong class="text-weight-bold">Presto.it</strong> {{ __('ui.PuoiComprareOnlineInModoSempliceESicuro')}}.</p>
                    <div class="d-flex no-padding col-10 col-lg-8">
                        <div class="input-group mb-4">
                            <form action="{{route('search_results')}}" method="GET">
                                <div class="input-group">
                                    <input type="search" class="form-control rounded search-bar" placeholder="{{ __('ui.CosaStaiCercando')}}" aria-label="Search" name="q"
                                      aria-describedby="search-addon" />
                                    <button type="submit" class="btn btn-red btn-red-no-padding ml-1"><i class="lni lni-search-alt mr-2"></i>{{ __('ui.Cerca')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img src="\img\header.jpg" class="img-responsive img-fluid" alt="">    
                </div>       
            </div>
        </div>
    </header>

    <section id="last-announcement" class="mt-5 bg-light-grey">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 mb-4">
                    <h3 class="text-blue h-section my-5">I nostri <span class="text-underline">ultimi</span> annunci.<a href="#" class="show-all ml-3 text-red">Vedi tutti<i class="lni lni-arrow-right align-text-bottom ml-2"></i></a></h3>
                </div>
            </div>
            <div class="row justify-content-start">
                {{-- PROVA CARDS --}}
                @foreach ($announcements as $announcement)
                <div class="col-lg-6">
                    <div class="card mb-3 p-1" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-4 p-2">
                                {{-- @dd($announcement->images->first()->file) --}}
                                @foreach ($announcement->images as $image)
                                    <img src="{{$image->getUrl(300, 300)}}" class=" card-img img-fluid " alt="...">
                                    @break
                                @endforeach 
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-8">
                                            <h5 class="card-title">
                                                <a href="{{route('announcement.show', compact('announcement'))}}"><strong>{{$announcement->title}}</strong>
                                                </a>
                                            </h5>
                                            <a class="label-category label-card text-blue" href="{{route ('public.announcements.category', [
                                                $announcement->category->name,
                                                $announcement->category->id,
                                            ])}}">{{$announcement->category->name}}
                                            </a>
                                        </div>
                                        <div class="col-4 text-right">
                                            <h6>
                                                <strong><span class="price">{{ $announcement->price}}</span><small>€</small></strong>
                                               
                                            </h6>
                                        </div>
                                    </div>
                                    <hr class="hr-card">
                                    <p class="card-text text-justify pr-2">
                                        {{Str::limit($announcement->body, 100)}}<a href="{{route('announcement.show', compact('announcement'))}}" class="text-blue"><strong>vedi l'annuncio</strong></a>
                                    </p>
                                    <div class="row mt-2">
                                        <div class="col-lg-10 bg-white">
                                            <a href="{{route('announcement.show', compact('announcement'))}}" class="btn btn-card">Vedi annuncio</a>
                                        </div>
                                        <div class="col-lg-2 bg-white">
                                            <i class="far fa-heart"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
                
            </div>
        </div>
    </section>
</x-layouts>
