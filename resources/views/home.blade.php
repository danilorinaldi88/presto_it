<x-layouts>
<div class="container">
    <div class="row justify-content-center my-5 text-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Pannello login</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Ti sei loggato correttamente!
                </div>
                <p class="text-center mb-5">
                    <a href="{{route('home')}}" class="btn btn-red text-center">Torna all'homepage</a>
                </p>
            </div>
        </div>
    </div>
</div>
</x-layouts>