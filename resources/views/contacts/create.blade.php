<x-layouts>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 mb-5">
                <hr>
                <h2 class="text-center h-medium l-height-40">Diventa revisore di Presto.it</strong></h2>
                <p class="text-center">La tua candidatura verrà analizzata dal nostro team, ti daremo un feedback al più presto</p>
                <hr>
            </div>
        </div>


        <div class="row justify-content-center align-item-center">
            <div class="col-lg-6">
                <img src="img/request-revisor.svg" class="d-block mx-auto h-350">
            </div>
            <div class="col-12 col-lg-4 mr-auto">    
                <form method="post" action="{{route('contacts.store')}}">
                @csrf
                <div class="form-group">
                    <label for="name" >Nome e Cognome</label>
                    <input name="name" type="name" class="form-control" id="name" aria-describedby="name&surname" value="{{ Auth::user()->name }}" readonly>
                    @error('name') 
                        <div class="alert alert-danger">{{$message}}  </div>
                    @enderror
                </div>
                <div class="form-group mt-4">
                    <label for="exampleInputEmail1" class="mb-3">Indirizzo email</label>
                    <input type="email" name="email" class="form-control" id="email" aria-describedby="emai
                    lHelp"  value="{{ Auth::user()->email }}" readonly>
                    @error('email') 
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                    <p class="text-blue modify-user mt-4" >
                        Vuoi modificare i tuoi dati?<a href="{{route('profile.edit', 'user')}}"></span> Clicca qui</a>
                    <p>
                    <button type="submit" class="btn btn-red">Invia Richiesta</button>
                </form>
            </div>
        </div>
    </div>
</x-layouts>




