<x-layouts>
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-12 col-md-6">
                <hr>
                <h2 class="text-center h-medium l-height-50">I risultati della tua ricerca: <strong>{{$q}}</strong></h2>
                </p>
                <hr>
            </div>
        </div>
        <div class="row justify-content-center">
            {{-- @foreach ($announcements as $announcement)
                <div class="col-lg-10">
                    <div class="card card-body mb-4">
                        <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                            <div class="mr-2 mb-3 mb-lg-0"> <img class="rounded img-fluid img-responsive" src="https://via.placeholder.com/160" width="160" height="160" alt=""> </div>
                            <div class="media-body">
                                <h5 class="media-title card-title"><a href="{{route('announcement.show', compact('announcement'))}}">{{ $announcement->title}}</h5>
                                <ul class="list-inline list-inline-dotted mb-3 mb-lg-2">
                                    <li class="list-inline-item text-blue"><a href="{{route ('public.announcements.category', [
                                        $announcement->category->name,
                                        $announcement->category->id,
                                    ])}}" class="text-blue card-category" data-abc="true"><span class="card-label">{{$announcement->category->name}}</span></a></li>
                                </ul>
                                <p class="mb-3 card-description">{{Str::limit($announcement->body, 100)}}<a href="{{route('announcement.show', compact('announcement'))}}" class="ads-read-more">vedi l'annuncio</a>
                                </p>
                                <ul class="list-inline list-inline-dotted mb-0">
                                    <li class="list-inline-item card-user">Inserito da: <strong>{{$announcement->user()->first()->name}}</strong></li>
                                    <li class="list-inline-item card-date">Data inserimento: <strong>{{$announcement->created_at->format('d/m/Y')}}</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            
            @endforeach --}}


            {{-- Prova --}}
            @foreach ($announcements as $announcement)
            <div class="col-lg-6">
                <div class="card mb-3 p-1" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4 p-2">
                            @foreach ($announcement->images as $image)
                                <img src="{{$image->getUrl(300, 300)}}" class=" card-img img-fluid " alt="...">
                                @break
                            @endforeach 
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="card-title">
                                            <a href="{{route('announcement.show', compact('announcement'))}}"><strong>{{$announcement->title}}</strong>
                                            </a>
                                        </h5>
                                        <a class="label-category label-card text-blue" href="{{route ('public.announcements.category', [
                                            $announcement->category->name,
                                            $announcement->category->id,
                                        ])}}">{{$announcement->category->name}}
                                        </a>
                                    </div>
                                    <div class="col-4 text-right">
                                        <h6>
                                            <strong><span class="price">99</span><small>€</small></strong>
                                        </h6>
                                    </div>
                                </div>
                                <hr class="hr-card">
                                <p class="card-text text-justify pr-2">
                                    {{Str::limit($announcement->body, 100)}}<a href="{{route('announcement.show', compact('announcement'))}}" class="text-blue"><strong>vedi l'annuncio</strong></a>
                                </p>
                                <div class="row mt-2">
                                    <div class="col-lg-10 bg-white">
                                        <a href="{{route('announcement.show', compact('announcement'))}}" class="btn btn-card">Vedi annuncio</a>
                                    </div>
                                    <div class="col-lg-2 bg-white">
                                        <i class="far fa-heart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</x-layouts>