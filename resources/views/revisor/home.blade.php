<x-layouts>
    <x-slot name="title">Annunci da revisionare | Presto.it</x-slot>
    @if($announcement !== null)
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-md-6">
                    <hr>
                    <h1 class="h-medium py-3"><strong>{{Auth::user()->name}}</strong>, hai questi annunci da revisionare:</h1>
                    <p>Puoi cliccare sul titolo dell'annuncio per visualizzare i dettagli.</p>
                </div>
            </div>
        </div>
        
        <div class="container b-light-grey">
            <div class="row justify-content-center mt-3 py-3 card-accept">
                <div class="col-lg-12">
                    <div class="row justify-content-center mb-5">
                        <div class="col-lg-2">
                            <form action="{{ route('revisor.accept' , $announcement->id)}}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-accept" >
                                    <i class="fas fa-check mr-2"></i>Accetta
                                </button>
                            </form>
                        </div>
                        <div class="col-lg-2">
                            <form action="{{ route('revisor.reject' , $announcement->id)}}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-reject" >
                                    <i class="fas fa-times mr-2"></i>Rifiuta
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center mb-5">
                <div class="col-12 col-lg-3">
                    <p><span class="label-title-r">Titolo dell'annuncio</span></p>
                    <h6 class="label-revisor mb-3">{{ $announcement->title}}</h6>
                    <p><span class="label-title-r">Categoria</span></p>
                    <h6 class="label-revisor">{{$announcement->category->name}}</h6>
                    </h6>
                </div>
                <div class="col-12 col-lg-3">
                    <p><span class="label-title-r">Annuncio inserito da:</span></p>
                    <h6 class="label-revisor mb-3">{{$announcement->user()->first()->name}}</h6>
                    <p><span class="label-title-r">Data inserimento</span></p>
                    <h6 class="label-revisor">{{$announcement->created_at->format('d/m/Y')}}</h6>
                    </h6>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                    @foreach ($announcement->images as $image)
                    <div class="row mb-3">
                        <div class="col-6 mt-3">
                    
                            <img src="{{$image->getUrl(300,300)}}" class="rounded h-200 d-block mx-auto img-fluid img-responsive" alt="">
                        </div>
                        <div class="col-6">
                            <h6 class="text-weight-bold mt-2">Immagine vietata ai minori</h6>
                            @include('components/_SafeSearchBar', ['classification' => $image->adult])
                            <h6 class="text-weight-bold mt-2">Salute</h6>
                            @include('components/_SafeSearchBar', ['classification' => $image->medical])
                            <h6 class="text-weight-bold mt-2">Presa in giro</h6>
                            @include('components/_SafeSearchBar', ['classification' => $image->spoof])
                            <h6 class="text-weight-bold mt-2">Violenza</h6>
                            @include('components/_SafeSearchBar', ['classification' => $image->violence])
                            <h6 class="text-weight-bold mt-2">Immagine Spinta</h6>
                            @include('components/_SafeSearchBar', ['classification' => $image->racy])
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <div class="container my-5">
            <div class="row justify-content-center">
                <div class="col-lg-6 pb-5">
                    <hr>
                    <h1 class="h-medium text-center l-height-40">Complimenti <strong>{{Auth::user()->name}}, hai finito gli annunci da revisionare!</h3>
                    <hr>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12 my-2">
                    <img src="\img\revisor_ok.png" class="img-fluid img-responsive h-350 mx-auto d-block"alt="revisor finish">
                    <p class="text-center mb-5">
                        <a href="{{route('home')}}" class="btn btn-red text-center">Torna all'homepage</a>
                    </p>
                </div>
            </div>
        </div>
        
    @endif
</x-layouts>
    