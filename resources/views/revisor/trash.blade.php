<x-layouts>
    <x-slot name="title">Cestino degli annunci | Presto.it</x-slot>
    @if(\App\Models\Announcement::TrashedCount() !== 0)
    <div class="container py-5">
        <div class="row justify-content-center text-center">
            <div class="col-md-6">
                <hr>
                <h1 class="h-medium py-3"><strong>{{Auth::user()->name}}</strong>, puoi recuperare questi annunci precedentemente revisionati:</h1>
                <p>Puoi cliccare sul titolo dell'annuncio per visualizzare i dettagli.</p>
                {{-- <h1>Annuncio # {{$announcement->id}}</h1>
                <p># {{$announcement->user->id}} , {{$announcement->user->name}} , {{$announcement->user->email}} ,</p>
                --}}
                <hr>
            </div>
        </div>
      
          
        
          
      
        
    
     
        @foreach ($announcements as $announcement)
                    <div class="row justify-content-center">
                        <div class="col-lg-1">
                            <form action="{{ route('revisor.recovery' , $announcement->id)}}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-success" >Recupera</button>
                            </form>
                        </div>
                        <div class="col-lg-1">
                        


                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter">
                                    Elimina
                                </button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    {{--    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div> --}}
                                        <div class="modal-body">
                                        <h5 class="text-center">Sei sicuro di voler eliminare l'annuncio?</h5>
                                        </div>
                                        <div class="modal-footer justify-content-center">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                                        <form action="{{ route('revisor.delete' , compact('announcement'))}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger" >Elimina</button>
                                        </form>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                        </div>

                    </div>
                    <div class="row justify-content-center my-5">
                        <div class="col-12 col-md-6">
                            <div class="card card-body mb-4">
                                <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                    <div class="mr-2 mb-lg-0 align-middle"> 
                                        
                                        
                                    <div class="media-body">
                                        <div class="row justify-content-between">
                                            <div class="col-5">
                                            <h5 class="media-title card-title"><a href="{{route('announcement.show', compact('announcement'))}}">{{ $announcement->title}}</h5> 
                                            <h6 > <strong>{{ $announcement->price}}€ </strong></h6>      
                                            </div>
                                        </div> {{-- <hr> --}}
                                       
                                            
                                        <ul class="list-inline list-inline-dotted mb-3 mb-lg-2">
                                            <li class="list-inline-item text-blue"><a href="{{route ('public.announcements.category', [
                                                $announcement->category->name,
                                                $announcement->category->id,
                                            ])}}" class="text-blue card-category" data-abc="true"><span class="card-label">{{$announcement->category->name}} </span></a></li>
                                        </ul>
                                        <p class="mb-3 card-description">{{$announcement->body}}<a href="{{route('announcement.show', compact('announcement'))}}" class="ads-read-more">vedi l'annuncio</a>
                                        </p>
                                        <ul class="list-inline list-inline-dotted mb-0">
                                            <li class="list-inline-item card-user">Inserito da: <strong>{{$announcement->user()->first()->name}}</strong></li>
                                            <li class="list-inline-item card-date">Data inserimento: <strong>{{$announcement->created_at->format('d/m/Y')}}</strong></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
       @else
        <div class="container my-5">
            <div class="row justify-content-center">
                <div class="col-lg-6 pb-5">
                    <hr>
                    <h1 class="h-medium text-center l-height-40"><strong>{{Auth::user()->name}} non hai più annunci nel cestino</h3>
                    <hr>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12 my-2">
                    <img src="\img\revisor_ok.png" class="img-fluid img-responsive h-350 mx-auto d-block"alt="revisor finish">
                    <p class="text-center mb-5">
                        <a href="{{route('home')}}" class="btn btn-red text-center">Torna all'homepage</a>
                    </p>
                </div>
            </div>
        </div>
        
    @endif 
        
</x-layouts>