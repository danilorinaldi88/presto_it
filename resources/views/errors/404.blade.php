<x-layouts>
    <x-slot name="title">Pagina non trovata! Presto.it</x-slot>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6">
                <hr>
                <h2 class="text-center h-medium l-height-40"><strong>OPS!</strong> Pagina non trovata</strong></h2>
                <hr>
            </div>
        </div>
            <div class="row">
                <div class="col-12">
                    <img src="img/tenor.gif" alt="" class="d-block mx-auto">
                </div>
                <div class="col-12">
                    <p class="text-center my-5">
                        <a href="{{route('home')}}" class="btn btn-red text-center">Torna all'homepage</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</x-layouts>