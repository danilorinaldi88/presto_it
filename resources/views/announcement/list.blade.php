<x-layouts>

        <x-slot name="title">Lista degli annunci pubblicati | Presto.it</x-slot>
       
        @if(Auth::user()->announcements()->first() !== null)  
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-md-6">
                    <hr>
                    <h1 class="h-medium py-3"><strong>{{Auth::user()->name}}</strong>, ecco gli annunci che hai pubblicato su Presto.it</h1>
                    <p>Puoi cliccare sul titolo dell'annuncio per visualizzare i dettagli.</p>
                    {{-- <h1>Annuncio # {{$announcement->id}}</h1>
                    <p># {{$announcement->user->id}} , {{$announcement->user->name}} , {{$announcement->user->email}} ,</p>
                    --}}
                    <hr>
                </div>
            </div>

            <div class="row justify-content-center my-5">
          {{--   @foreach (Auth::user()->announcements()->get() as $announcement) --}}
         
            @foreach ($announcements as $announcement)
             @if ($announcement->is_accepted !==0 )
                
            
                                <div class="col-lg-6">
                                    <div class="card mb-3 p-1" style="max-width: 540px;">
                                        <div class="row no-gutters">
                                            <div class="col-md-4 p-2">
                                                @foreach ($announcement->images as $image)
                                                <img src="{{$image->getUrl(400, 350)}}"  alt="" class="img-fluid">
                                                @break
                                            @endforeach 
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-8">
                                                            <h5 class="card-title">
                                                                <a href="{{route('announcement.show', compact('announcement'))}}"><strong>{{$announcement->title}}</strong>
                                                                </a>
                                                            </h5>
                                                            <a class="label-category label-card text-blue" href="{{route ('public.announcements.category', [
                                                                $announcement->category->name,
                                                                $announcement->category->id,
                                                            ])}}">{{$announcement->category->name}}
                                                            </a> <br>
                                                            @if($announcement->is_accepted == null)
                                                                <span class="label-category label-card text-red">Non revisionato</span>
                                                        
                                                            @endif
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <h6>
                                                                <strong><span class="price">{{$announcement->prices}}</span><small>€</small></strong>
                                                            </h6>
                                                        </div>
                                                    </div>
                                                    <hr class="hr-card">
                                                    <p class="card-text text-justify pr-2">
                                                        {{Str::limit($announcement->body, 100)}}<a href="{{route('announcement.show', compact('announcement'))}}" class="text-blue"><strong>vedi l'annuncio</strong></a>
                                                    </p>
                                                    <div class="row mt-2">
                                                        <div class="col-lg-3 bg-white">
                                                            <form action="{{ route('announcement.edit' , compact('announcement'))}}" method="GET">
                                                            @csrf
                                                                    <button type="submit" class="btn btn-card">
                                                                        Modifica
                                                                    </button>
                                                            </form>
                                                        
                                                        </div>
                                                        <div class="col-lg-3 bg-white">
                                                            <button type="button" class="btn btn-card-red" data-toggle="modal" data-target="#elimina-{{$announcement->id}}">
                                                                Elimina
                                                            </button>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- Modal -->
                                <div class="modal fade" id="elimina-{{$announcement->id}}" {{-- tabindex="-1" role="dialog"  --}}aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                    <h5 class="text-center">Sei sicuro di voler eliminare l'annuncio?</h5>
                                    </div>
                                    <div class="modal-footer justify-content-center">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                                    
                                    <form action="{{route('announcement.delete' , compact('announcement'))}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" >Elimina</button>
                                        
                                    </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                            
                            @endif
                            
                          
                        @endforeach
                    </div>

           @else
           
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 pb-5">
                        <hr>
                        <h1 class="h-medium text-center l-height-40"><strong>{{Auth::user()->name}}, non hai annunci da visualizzare</h3>
                        <hr>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="row justify-content-center align-item-center">
                        <div class="col-lg-6 my-2">
                            <img src="\img\no-article-list.svg" class="img-fluid mx-auto d-block h-200"alt="revisor finish">
                        </div>
                        <div class="col-lg-6 pr-5 py-3">
                            <h6>Crea il tuo primo annuncio su Presto.it</h6>
                            <p>E' facile, gratuito e senza commissioni sia per chi vende che per chi compra!</p>
                            <p class="mb-5">
                                <a href="{{route('announcement.new')}}" class="btn btn-red-outline"><i class="lni lni-add-files mr-2"></i>Crea nuovo annuncio</a>
                            </p>
                            <p>
                            </p>
                        </div-col-6>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <p class="text-center mb-5">
                                <a href="{{route('home')}}" class="btn btn-red text-center">Torna all'homepage</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        @endif 
            
    
    </x-layouts>
