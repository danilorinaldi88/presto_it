<x-layouts>
  
    <x-slot name="title">Crea Annuncio | Presto.it</x-slot>
    
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-6 mb-5">
            <hr>
            <h2 class="text-center h-medium l-height-40">Modifica il tuo annuncio su Presto.it!<br>{{-- DEBUG::  SECRET {{$uniqueSecret}} --}}</strong></h2>
            <hr>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-12 col-lg-6">
          <form method="POST" action="{{route('announcement.update', compact('announcement'))}}">
            @csrf
            @method('PUT')
            <input type="hidden" name='uniqueSecret' value="{{$uniqueSecret}}">
            @if (session('message'))
                    <div class="alert alert-success">
                        <p> {{session('message')}} </p>
                    </div>
            @endif
            @if (session('announcement.created.success'))
            <div class="alert alert-success">
              Annuncio creato con successo!
            </div>                        
            @endif
            <div class="form-group">
              <label for="title">Titolo dell'annuncio</label>
              <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"
              id="title"  value="{{$announcement->title}}"required autofocus>
              
              @error('title')
              <span class="invalid-feedback" role="alert">
                <strong>{{$message}}</strong>
              </span>
              @enderror
              
            </div>
            <div class="form-group">
              <label for="body">Corpo dell'annuncio</label>
              <textarea class="form-control @error('title') is-invalid @enderror" 
              for="body" cols="10" rows="6" name="body" 
              required autofocus>
              {{$announcement->body}}
            </textarea>
            
            @error('body')
            <span class="invalid-feedback" role="alert">
              <strong>{{$message}}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="category">Categoria</label>
            <div>
              <select name="category" id="category" class="custom-select">
                @foreach ($categories as $category)
                <option value="{{$category->id}}" {{old('category')==$category->id ? 'selected' : ''}}>{{$category->name}}
                </option>
              @endforeach                          
              </select>
              @error('category')
              <span class="invalid-feedback" role="alert">
                <strong>{{$message}}</strong>
              </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label for="images" class="col-md-12 col-form-label text-md-left">Immagini</label>
            <div class="col-md-12">
                <div class="dropzone" id="drophere"></div>
                @error('images')
                  <span class="invalid-feedback" role="alert" >
                      <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
          </div>
          <button type="submit" class="btn btn-red">Crea annuncio</button>
        </form>
      </div>
    </div>
  </div>

  




 @if($announcement->images->first()!== null)
  <div class="container">
    <div class="row justify-content-center mt-5">
      <div class="col-12 col-md-4">
        <h2 class="h-medium py-3"><strong>Immagini caricate</strong></h2>
      </div>
    </div>
    <div class="col-12 col-md-12">
      <form action="{{route('image.delete', $announcement->images->first())}}" method="post">
        @csrf
        @method('DELETE')
        <div class="row justify-content-center">
          @foreach ($announcement->images as $image)
          <div class="col-3 col-md-3 max-width position-relative">
            <img  src="{{$image->getUrl(400, 350)}}"  alt="" class="img-fluid">
            <button class="btn btn-block text-danger position"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
              <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
            </svg></button>
          </div>
          @endforeach
        
        </form>
      </div>
    </div>
  </div>
  @else
  <div class="container">
    <div class="row justify-content-center text-center mt-5">
      <div class="col-12 col-md-6">
        <h2 class="h-medium py-3"> <strong>Non hai caricato immagini per questo articolo </strong></h2>
      </div>
    </div>
  @endif
 

  </x-layouts>