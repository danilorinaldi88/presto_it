<x-layouts>
  
  <x-slot name="title">Crea Annuncio | Presto.it</x-slot>
  
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-lg-6 mb-5">
          <hr>
          <h2 class="text-center h-medium l-height-40">Crea il tuo annuncio su Presto.it!<br>{{-- DEBUG::  SECRET {{$uniqueSecret}} --}}</strong></h2>
          <hr>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-12 col-lg-6">
        <form method="POST" action="{{route('announcement.create')}}">
          @csrf
          <input type="hidden" name='uniqueSecret' value="{{$uniqueSecret}}">
          @if (session('announcement.created.success'))
          <div class="alert alert-success">
            Annuncio creato con successo!
          </div>                        
          @endif
          <div class="form-group">
            <label for="title">Titolo dell'annuncio</label>
            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"
            id="title" placeholder="Inserisci il titolo" value="{{old('title')}}"required autofocus>
            
            @error('title')
            <span class="invalid-feedback" role="alert">
              <strong>{{$message}}</strong>
            </span>
            @enderror
            
          </div>
          <div class="form-group">
            <label for="body">Corpo dell'annuncio</label>
            <textarea class="form-control @error('title') is-invalid @enderror" 
            for="body" cols="10" rows="6" name="body" 
            required autofocus>
            {{old('body')}}
            </textarea>
          
            @error('body')
            <span class="invalid-feedback" role="alert">
              <strong>{{$message}}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group">
            <label for="category">Categoria</label>
            <select name="category" id="category" class="custom-select">
              @foreach ($categories as $category)
              <option value="{{$category->id}}"{{old('category')==$category->id ? 'selected' : ''}}>{{$category->name}}
              </option>
              @endforeach                          
            </select>
              @error('category')
                <span class="invalid-feedback" role="alert">
                <strong>{{$message}}</strong>
                </span>
              @enderror
          </div>

          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">€</span>
            </div>
            <input name="price" type="number" class="form-control" aria-label="Amount (to the nearest dollar)">
            <div class="input-group-append">
              <span class="input-group-text">.00</span>
            </div>
            @error('price')
                <span class="invalid-feedback" role="alert">
                <strong>{{$message}}</strong>
                </span>
              @enderror
          </div>

          <div class="form-group row">
            <label for="images" class="col-md-12 col-form-label text-md-left">Immagini</label>
            <div class="col-md-12">
                <div class="dropzone" id="drophere"></div>

                @error('images')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
          </div>
          <button type="submit" class="btn btn-red">Crea annuncio</button>
        </form>
      </div>
    </div>
  </div>

</x-layouts>