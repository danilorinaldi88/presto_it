<x-layouts>
    <x-slot name="title">{{$announcement->title}} | Presto.it</x-slot>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 mb-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white" id="bread">
                    <li class="breadcrumb-item text-blue"><a class="text-blue" href="{{ url('/') }}"><i class="fas fa-chevron-right mr-2"></i>Home</a></li>
                    <li class="breadcrumb-item text-blue"><a class="text-blue" href="{{route ('public.announcements.category', [
                        $announcement->category->name,
                        $announcement->category->id,
                    ])}}"><i class="fas fa-chevron-right mr-2"></i>{{$announcement->category->name}}</a></li>
                    <li class="breadcrumb-item text-blue" aria-current="page"><strong><i class="fas fa-chevron-right mr-2"></i>{{$announcement->title}}</strong> </li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-4">
                <div class="my-carousel">
                    @foreach ($announcement->images as $image)
                        <div>
                            <img src="{{$image->getUrl(300,300)}}" class="d-block mx-auto"alt="Image 1">
                        </div>
                        @endforeach
                </div>
            </div>
            <div class="col-12 col-lg-6 pl-5">
                <h3 class="announcement-subtitle text-uppercase mb-3 mt-2"><span class="label-category"><a href="{{route ('public.announcements.category', [
                    $announcement->category->name,
                    $announcement->category->id,
                ])}}">{{$announcement->category->name}}</span></a></h3>
                <p class="announcement-user-date">Annuncio inserito da: <strong>{{$announcement->user()->first()->name}}</strong> | Data inserimento: <strong>{{$announcement->created_at->format('d/m/Y')}}</strong></p>
                <hr>
                <h1 class="announcement-title"> <strong>{{ $announcement->price}}€ </strong></h1>  <hr>
                <h1 class="announcement-title"><strong>{{ $announcement->title}}</strong></h1><hr>
                
                <p class="announcement-body mt-3 text-justify">{{$announcement->body}}</p>
                
            </div>
        </div>    
    </div>
  
    <script>
        $('.my-carousel').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            dots:true,
            dotsClass: 'slick-dots',
            mobileFirst: true,
        });
    </script>
  </x-layouts>


