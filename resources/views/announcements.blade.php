<x-layouts>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 mb-5">
                <hr>
                <h2 class="text-center h-medium l-height-40">Risultati per la categoria: <strong>{{$category->name}}</strong></h2>
                <hr>
            </div>
        </div>
            <div class="row justify-content-center">
            @foreach ($announcements as $announcement)
            <div class="col-lg-6">
                <div class="card mb-3 p-1" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-4 p-2">

                            @foreach ($announcement->images as $image)

                            <img src="{{$image->getUrl(300, 300)}}" class=" card-img img-fluid " alt="...">
                                @break
                            {{-- <div>
                                    {{$image->id}} <br>
                                    {{$image->file}} <br>
                                    {{Storage::url($image->file)}} <br>


                                </div> --}}
                            @endforeach

                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="card-title">
                                            <a href="{{route('announcement.show', compact('announcement'))}}"><strong>{{$announcement->title}}</strong>
                                            </a>
                                        </h5>
                                        <a class="label-category label-card text-blue" href="{{route ('public.announcements.category', [
                                            $announcement->category->name,
                                            $announcement->category->id,
                                        ])}}">{{$announcement->category->name}}
                                        </a>
                                    </div>
                                    <div class="col-4 text-right">
                                        <h6>
                                            <strong><span class="price">{{ $announcement->price}}</span><small>€</small></strong>
                                        </h6>
                                    </div>
                                </div>
                                <hr class="hr-card">
                                <p class="card-text text-justify pr-2">
                                    {{Str::limit($announcement->body, 100)}}<a href="{{route('announcement.show', compact('announcement'))}}" class="text-blue"><strong>vedi l'annuncio</strong></a>
                                </p>
                                <div class="row mt-2">
                                    <div class="col-lg-10 bg-white">
                                        <a href="{{route('announcement.show', compact('announcement'))}}" class="btn btn-card">Vedi annuncio</a>
                                    </div>
                                    <div class="col-lg-2 bg-white">
                                        <i class="far fa-heart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-8">
                {{$announcements->links()}}
            </div>
        </div>
    </div>

</x-layouts>