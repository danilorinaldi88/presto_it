>>>> To do list >>>>>
> Aggiungere modale per il login e la registrazione / Capiere come fare!!
> else di lista annunci
> Inserire search bar su tutte le viste

> Cambio watermark
> Hover effect nel menù




>>>> Working Progress >>>>>

> Modifica di tutti i title delle pagine


>>>> Done >>>>>

> Creazione vista annuncio singolo
> Aggiunta nella navbar del link lavora con noi solo per utente registrati
> Aggiunta possibilità di modifica dati dell’utente
> Aggiunta possibilità per l’utente di vedere tutti i suoi annunci
> Aggiunta possibilità per l’utente di modificare il suo annuncio
> Eliminata voce di menù "Lavora con noi" per i revisori (da ottimizzare)
> Eliminato dropdown nel menù con le categorie
> Ottimizzato menù versione mobile (Categorie rippare con il menù ad hamburger)
> Pagina di ringraziamento per aver inviato richiesta revisore
> Fixato front-end pagina "Hai revisionato tutti gli articoli"
> Fixato front-end pagina revisione articoli
> Fixato front-end pagina cestino annunci
> Creazione pagina di cestino annunci
> Fixato front-end pagina reset password
> Ammodernare la mail
> Aggiungere broadcrumb negli annunci
> Fixare messaggio quando elimini definitivamente o recuperi un annuncio su modale
> Creazione pannello admin con privilegi di modifica da utente normale a revisore
> Aggiungere ID e indirizzo mail nella vista del revisore per accettare gli articoli
> Aggiungere possibilità in tutte le viste con più annunci, che se c'è un solo annuncio venga visualizzato in una row, se ce n'è più di uno invece si dispongano in colonne
> Modificare il form della vista edit annuncio
> Creazione link sotto al form di invio richiesta per diventare revisore con scritto “vuoi modificare i tuoi dati?”
> Far inviare una mail automatica quando diventa revisore
> Sistemata vista lista annunci
> Sistemata search bar nella homepage
> Sistemato effetto hover nel link della navbar
> Sistemare link interno lista annunci (il link prende il // doppio slash)
> Aggiungere etichetta "non ancora revisionato" nella vista lista annunci
> Cambiare testo della mail automatica al revisore