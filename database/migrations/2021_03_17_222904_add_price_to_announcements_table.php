<?php

use App\Models\Announcement;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceToAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->integer('price')->after('body')->nullable();
        });

        $articles = [
            ['title'=>'Fiat Panda', 'body'=>'Panda Hybrid, con Clima e Radio Dab! Da 8.400€ anziché 9.900€. In caso di rottamazione. Scegli ora e paga a Gennaio 2022. TAN 6,85% - TAEG 10,00%. Scopri di più! Sorprendente. Ideale per la Città. Comfort e Sicurezza. Agile e Versatile.', 'price'=>'8400','categoria'=>1],
            ['title'=>'Il codice Davinci', 'body'=>'Il codice da Vinci (The Da Vinci Code) è un film thriller del 2006, diretto da Ron Howard e basato sull\'omonimo romanzo best seller di Dan Brown.
            Il film, distribuito dalla Sony Pictures Entertainment, è uscito in contemporanea nelle sale di tutto il mondo il 19 maggio 2006, dopo l\'anteprima al Festival di Cannes 2006 il 16 maggio.', 'price'=>'34','categoria'=>4],
            ['title'=>'Bicicletta', 'body'=>'Con il nuovo telaio della gamma EDR (EnDuRance), questa bici da corsa ultra performante è ideata per accompagnare i ciclisti più esigenti in tutte le uscite.
             Adatto sia in montagna sia su terreni misti, la nervosità ed il comfort di questo nuovo telaio ti sorprenderanno.Bici assemblata nella Fiandre, a Lille, nel nord della Francia.', 'price'=>'199','categoria'=>1],
             ['title'=>'Mac Book Pro', 'body'=>'Chip Apple M1 con CPU 8‑core e GPU 8‑core
             Archiviazione 512GB, Chip Apple M1 con CPU 8‑core, GPU 8‑core e Neural Engine 16‑core, 8GB di memoria unificata, Archiviazione SSD da 512GB¹,Display Retina da 13" con True Tone, Magic Keyboard. Touch Bar e Touch ID, Trackpad Force Touch, Due porte Thunderbolt / USB 4', 'price'=>'790','categoria'=>2],
             ['title'=>'Ciccio Bello', 'body'=>'Cicciobello Amicicci sono piccoli bebè morbidosi e la loro testa dondola; fanno movimenti e hanno tante espressioni buffe e divertenti; scoprilo premendo le loro guance o il mento
             Sono inclusi una divertente altalena e tantissimi accessori sia per il momento del gioco che per la merenda', 'price'=>'11','categoria'=>5],
             ['title'=>'Apple iPhone 11 Pro', 'body'=>'iPhone 11 Pro e iPhone 11 Pro Max sono gli smartphone più evoluti e potenti che abbiamo mai realizzato. Sono dotati di tecnologie all’avanguardia su cui potranno fare affidamento sia i professionisti sia tutti gli altri utenti che desiderano il miglior dispositivo di sempre,” ha dichiarato Phil Schiller, Senior Vice President Worldwide Marketing di Apple.', 'price'=>'599','categoria'=>8],
             ['title'=>'Centrifuga Professionale Inox, 400 Severin ES 356', 'body'=>'la centrifuga Severin ES 3566 è un alleato quotidiano per il gusto e il benessere di tutta la famiglia. Le sue principali caratteristiche sono: motore professionale, adatto ad un utilizzo intensivo; motore Supersilent, silenzioso e raffreddato ad aria; rotazione fino a 19.000giri/min.', 'price'=>'130','categoria'=>3],
             ['title'=>'Ecoscandaglio Garmin echomap chirp 72 sv', 'body'=>'Strumento combinato fishfinder/chartplotter da 7" con funzioni ClearVü(TM) e SideVü(TM)
             Ricevitore GPS da 5 Hz integrato
             Ecoscandaglio integrato da 500 W (RMS) con tecnologia HD-ID(TM) (4.000 W picco-picco)
             ecnologia Garmin ClearVü e SideVü¹
             Staffa di supporto basculante inclusa', 'price'=>'199','categoria'=>6],
             ['title'=>'Immobile ad uso ufficio ad Olmo di Martellago', 'body'=>'Proponiamo in vendita ufficio di circa 70 mq, posto al primo piano e composto da ingresso su sala d’attesa di c.ca 20 mq, 2 luminose stanze ad uso ufficio, rispettivamente di 28 e 10 mq e doppi servizi. zona centrale e con parcheggio adiacente.
             classe energetica in definizione.
             rif. 4014
             euro 70.000', 'price'=>'70.000','categoria'=>7],
             ['title'=>'Trilogia il signore degli Anelli', 'body'=>'Il Signore degli anelli è un romanzo d\'eccezione, al di fuori del tempo. E un libro d\'avventure in luoghi remoti e terribili, episodi di inesauribile allegria, segreti paurosi che si svelano a poco a poco, draghi crudeli e alberi che camminano, città d\'argento e di diamante poco lontane da necropoli tenebrose in cui dimorano esseri che spaventano solo al nominarli, urti giganteschi di eserciti luminosi e oscuri. ', 'price'=>'100','categoria'=>4],
             ['title'=>'Sale con tavolo e 4 sedie', 'body'=>'vendo sala con tavolo e 4 sedie, in legno, tenuta benissimo, misure 3 metri x 2,30 altezza, tavolo 1x1 apribile come da foto. prezzo 250 euro, i mobili si trovano a castel mella (bs), facile smontaggio. ', 'price'=>'49','categoria'=>9],
             ['title'=>'Vespa ET4 150 1999 leader', 'body'=>'Vespa ET4 150 leader 1999 con bauletto tagliandata, forcella revisionata con ammortizzatore yss gommata e revisionata.
             Normali segni di usura dovuti al tempo guidata da mia moglie. 400 euro non trattabili. Non scambio non regalo. ', 'price'=>'1450','categoria'=>1],
             ['title'=>'Honda SH 300 Abs 2017 Permute', 'body'=>'Honda SH 300 Abs unico proprietario tagliandato e gommato finanziamento tasso agevolato garanzia 1 anno permute scooter moto e auto', 'price'=>'1980','categoria'=>1],
             ['title'=>'Bilocale Indipendente Etnea Caronda Sisto', 'body'=>'Bilocale Indipendente Etnea Caronda Sisto situato in Via Sisto a 20 metri dalla Via Caronda ed a 50 metri dalla Via Etnea in zona Feltrinelli proponiamo eccellente soluzione per uso investimento o abitazione e precisamente: Bilocale Indipendente di mq 65 composto da salone, camera, cucina abitabile e bagno oltre piccolo terazzino interno privato. L\'appartamento è completamente da ristrutturare e si presta a qualsiasi uso abitativo compreso Casa Vacanze o piede a terre, Via Sisto 18- Adiacente Via Caronda e Via Etnea.', 'price'=>'180.000','categoria'=>7],
             ['title'=>'Scarponi da sci TECNICA taglia 40 misura 6 e 1/2', 'body'=>'Vendo scarponi da Sci Tecnica arancione e titanio, ganci regolabili. Usati poco. Vendita causa inutilizzo. Taglia 40 misura 6 e 1/2 e lunghezza cm. 294.', 'price'=>'210','categoria'=>6],
        ];
        foreach($articles as $article){
            Announcement::create([
                'title'=>$article['title'],
                'body'=>$article['body'],
                'price'=>$article['price'],
                'category_id'=>$article['categoria'],
                'user_id'=>1,
                'is_accepted'=>true,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
