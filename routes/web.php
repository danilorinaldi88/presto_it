<?php

use App\Models\Announcement;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index']);
Route::get('/category/{name}/{id}/announcements', [PublicController::class, 'announcementsByCategory'])->name('public.announcements.category');

//rotte lingue
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/crea_annuncio/new', [HomeController::class, 'newAnnouncement'])->name('announcement.new');
Route::post('/crea_annuncio/create', [HomeController::class, 'createAnnouncement'])->name('announcement.create');
Route::get('/annuncio/edit/{announcement}', [HomeController::class, 'editAnnouncement'])->name('announcement.edit');
Route::put('/annuncio/update/{announcement}', [HomeController::class, 'updateAnnouncement'])->name('announcement.update');
Route::delete('annuncio/delete/{announcement}', [HomeController::class, 'delete'])->name('announcement.delete');
Route::get('/annuncio/lista', [Homecontroller::class, 'list'])->name('announcement.list');
Route::get('/annuncio/{announcement}', [PublicController::class, 'showAnnouncement'])->name('announcement.show');

Route::get('/revisor/home/', [RevisorController::class, 'index'])->name('revisor.home');
Route::post('/revisor/announcement/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');
Route::post('/revisor/announcement/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');
Route::get('revisor/trash', [RevisorController::class, 'trash'])->name('revisor.trash');
Route::post('revisor/announcement/{id}/recovery', [RevisorController::class, 'recovery'])->name('revisor.recovery');

Route::delete('revisor/announcement/delete/{announcement}', [RevisorController::class, 'delete'])->name('revisor.delete');



Route::get('/lavora-con-noi', [ContactController::class, 'contactUs'])->name('contacts.create');
Route::post('/lavora-con-noi/store', [ContactController::class, 'contactUsStore'])->name('contacts.store');


Route::get('/search', [PublicController::class, 'search'])->name('search_results');

Route::get('/profilo', [ProfileController::class, 'create'])->name('profile.create');
Route::get('/profilo/edit/{user}', [ProfileController::class, 'edit'])->name('profile.edit');
Route::put('/profilo/update/{user}', [ProfileController::class, 'update'])->name('profile.update');
Route::delete('/profilo/delete/{user}', [ProfileController::class, 'delete'])->name('profile.delete');


Route::get('/admin', [RoleController::class, 'index'])->name('roles.admin');
Route::get('/admin/show/{user}', [RoleController::class, 'show'])->name('roles.show');
Route::put('/admin/update/{user}', [RoleController::class, 'update'])->name('roles.update');

Route::post('/announcement/images/upload', [HomeController::class, 'uploadImage'])->name('announcement.images.upload');
Route::delete('/announcement/images/remove', [HomeController::class, 'removeImage'])->name('announcement.images.remove');
Route::get('/annoucement/images', [HomeController::class, 'getImages'] )->name('announcement.images');

Route::delete('announcement/{image}/delete', [HomeController::class, 'deleteimage'])->name('image.delete');

Route::get('/auth/google', [GoogleController::class, 'redirectToGoogle'])->name('redirectToGoogle');
Route::get('/auth/google/callback', [GoogleController::class, 'handleGoogleCallback'])->name('handleGoogleCallback');
