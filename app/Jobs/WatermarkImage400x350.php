<?php

namespace App\Jobs;

use App\Models\AnnouncementImage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class WatermarkImage400x350 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $announcement_image_id;

    public function __construct($announcement_image_id)
    {
        $this->announcement_image_id = $announcement_image_id;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $i = AnnouncementImage::find($this->announcement_image_id);
        if(!$i) {
            return;
            }
        $originalPath = storage_path('app/' . $i->file);
        $dirPath = dirname($originalPath);
        $fileName = basename($originalPath);
    
        $srcPath_400x350 = $dirPath . '/crop400x350_' . $fileName;
        
        $image = Image::load($srcPath_400x350);
        $image->watermark(base_path('public/img/watermark.png'))
            ->watermarkWidth(150, Manipulations::UNIT_PIXELS)
            ->watermarkHeight(35, Manipulations::UNIT_PIXELS);
        $image->save($srcPath_400x350);

    }
}
