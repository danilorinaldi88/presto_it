<?php

namespace App\Http\Controllers;

use App\Mail\CambioDiRuolo;
use App\Mail\ContactSend;
use App\Models\Announcement;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.role');
    }
    public function index(){
        $users=User::all();
      
       return view('roles.admin', compact('users'));
    }
    public function show(User $user) {
    return view('roles.show', compact('user'));

    }

    public function update(Request $request, User $user)
    {
        
        if($request->role_id != $user->role_id){
            $name = $request->input('name');
            $email = $request->input('email');
            $role_id = $request->input('role_id');

            $contact = [
            'name'=>$name,
            'email'=>$email,
            'role_id'=>$role_id,
            ];
            Mail::to($email)->send(new CambioDiRuolo($contact));
         }

        $loguser= Auth::user();
        if($loguser->name == $user->name){
        $user->name = $request->name;
        $user->role_id =  1;
        $user->is_revisor = true;
        $user->email = $request->email;   
        $user->save();
        
        }
     //se sei un utente e vuoi passare a revisore o ad amministratore, se sei un amministratore e vuoi passare a revisore
        elseif($user->role_id == 0 || $user->role_id == 1  ){
                $user->name = $request->name;
                $user->role_id = $request->role_id;
                $user->email = $request->email;  
                $user->is_revisor= true;
                $user->save();
         }
         //se sei un revisore e passi ad essere amministratore
        elseif($user->role_id == 2 && $request->role_id == 1){
                $user->name = $request->name;
                $user->role_id = $request->role_id;
                $user->email = $request->email;  
                $user->is_revisor= true;
                $user->save();
        }
        else{
        // se sei un revisore e devi passare ad essere un utente semplice
                $user->name = $request->name;
                $user->role_id = $request->role_id;
                $user->email = $request->email;  
                $user->is_revisor= false;
                $user->save();
            
        }
        

    $users=User::all();
    return view('roles.admin', compact('users'));

    }

 
}
