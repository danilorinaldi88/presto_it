<?php

namespace App\Http\Controllers;

use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;
use App\Jobs\WatermarImage;
use App\Jobs\WatermarkImage;
use App\Jobs\WatermarkImage400x350;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function newAnnouncement(Request $request)
    {
        $uniqueSecret = $request->old(
            'uniqueSecret',
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );
            

        return view('announcement.new', compact('uniqueSecret'));
    }

    public function createAnnouncement(AnnouncementRequest $request)
    {
        $a = new Announcement();

        $a->title = $request->input('title');
        $a->body = $request->input('body');
        $a->category_id = $request->input('category');
        $a->user_id = Auth::id();
        $a->price = $request->input('price');
        $a->save();
        
        $uniqueSecret = $request->input('uniqueSecret');
      
      
        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedImages=session()->get("removeimages.{$uniqueSecret}",[]);
        
        
        $images = array_diff($images,$removedImages);
        
        foreach ($images as $image) {
            $i = new AnnouncementImage();

            $fileName = basename($image);
            $newFileName = "public/announcements/{$a->id}/{$fileName}";

            Storage::move($image, $newFileName);
            
            $i->file = $newFileName;
            $i->announcement_id = $a->id;
            
            $i->save();

            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new ResizeImage($i->file, 300, 300),
                new ResizeImage($i->file, 400, 350),
                new WatermarkImage($i->id),
                new WatermarkImage400x350($i->id),
            ])->dispatch($i->id);

            
        }
        
        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
        
        return redirect()->back()->with('announcement.created.success', 'ok');
        
    }

    public function uploadImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName, 
            120, 
            120
        ));

        session()->push("images.{$uniqueSecret}", $fileName);
        
        return response()->json(
            [
                'id'=> $fileName
            ]
        );

    }

    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');
       
        session()->push("removeimages.{$uniqueSecret}", $fileName);
        $removed = session()->get("removeimages.{$uniqueSecret}");
       
        Storage::delete($fileName);

        return response()->json('ok');

    }
    public function getImages(Request $request){
        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);

        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);
        
        $images = array_diff($images, $removedImages);

        $data = [];

        foreach($images as $image){
            $data[] = [
                'id'=>$image,
                'src'=>AnnouncementImage::getUrlByFilePath($image, 120, 120),
            ];
        }
        return response()->json($data);
    }


    public function editAnnouncement(Announcement $announcement, Request $request){
        $uniqueSecret = $request->old(
            'uniqueSecret',
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );
        return view('announcement.edit', compact('announcement', 'uniqueSecret'));
    }

    public function updateAnnouncement(Request $request, Announcement $announcement){
        
        $announcement->title = $request->title;
        $announcement->body = $request->body;
        $announcement->category_id = $request->category;
        $announcement->is_accepted = null;
        $announcement->save(); 
        
        $uniqueSecret = $request->input('uniqueSecret');

       
        $images = session()->get("images.{$uniqueSecret}",[]);

        $removedImages=session()->get("removeimages.{$uniqueSecret}",[]);  
        $images = array_diff($images,$removedImages);

        foreach ($images as $image) {
            $i = new AnnouncementImage();

            $fileName = basename($image);
            $newFileName = "public/announcements/{$announcement->id}/{$fileName}";

            Storage::move($image, $newFileName);
         
            $i->file = $newFileName;
            $i->announcement_id = $announcement->id;
            $i->save();

            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new ResizeImage($i->file, 300, 300),
                new ResizeImage($i->file, 400, 350),
                new WatermarkImage($i->id),
                new WatermarkImage400x350($i->id),
            ])->dispatch($i->id);       
        }
        
        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
        return redirect()->back()->with('message' , 'Articolo Modificato');

 }




    public function list()
    {
        $announcements=Auth::user()->announcements()->get();
        
        return view('announcement.list', compact('announcements'));
    }

    public function delete(Announcement $announcement)
    {
        $announcement->delete();
        return redirect(route('announcement.list'))->with('message' , 'hai cancellato il messaggio');
      
    }

    public function deleteimage(AnnouncementImage $image)
    {
        //url dell'immagine che voglio eliminare
        $splittedPath = explode('/', $image->file);
        //prendo l'ultimo pezzo della url
        $fileName = $splittedPath[count($splittedPath) - 1];
        
        //prendere tutte le immagini di questo annuncio
        //cercate tra le immagini quelle che hanno nel path, l'url dell'immagine
        $announcement_id = $image->announcement->id;
        $images = Storage::files("public/announcements/$announcement_id");
        //filtro tutte le immagini per prendermi solo quelle che contengo quel nome
        $filteredImages = collect($images)->filter(function($image) use($fileName){
            return str_contains($image, $fileName);
        }); 
        foreach ($filteredImages as $imageToDelete) {
            Storage::delete($imageToDelete);
        }
        $image->delete();
        return redirect()->back();
    }

}
