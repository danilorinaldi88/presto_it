<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactStoreRequest;
use App\Mail\ContactReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contactUs(){
    return view('contacts.create');   
    }

    public function contactUsStore(ContactStoreRequest $request){
           /*  dd('Ciao');
            $request->validate();
            $name = $request->input('name');
            $email = $request->input('email');
            $phone = $request->input('phone');


            $contact = [
            'name'=>$name,
            'email'=>$email,
            'phone'=> $phone,
            ];
            dd($contact); */

            
            $contact = $request->validated();
            Mail::to('lorenzo.gian@gmail.com')->send(new ContactReceived($contact));
            return view('mail.thankyou');
  
    }


}
