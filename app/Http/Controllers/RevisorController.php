<?php

namespace App\Http\Controllers;


use App\Models\Announcement;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\PseudoTypes\False_;

class RevisorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.revisor');
    }
     public function index()
    {
        $announcement = Announcement::where('is_accepted', null)
        ->orderBy('created_at', 'desc')
        ->first();
        return view('revisor.home', compact('announcement'));
    }
    private function setAccepted($announcement_id, $value)
    {
        $announcement = Announcement::find($announcement_id);
        $announcement->is_accepted = $value;
        $announcement->save();
        return redirect(route('revisor.home'));
    }

    public function accept($announcement_id)
    {
        return $this->setAccepted($announcement_id, true);
    }

    public function reject($announcement_id)
    {
        // $announcement = Announcement::find($announcement_id)->first();
        // $announcement->is_accepted = null;
        // $announcement->save();
        // return redirect(route('revisor.home'));
        return $this->setAccepted($announcement_id, false); 
    }

    public function trash()
    {
        $announcements = Announcement::where('is_accepted', false)->get();
        return view('revisor.trash', compact('announcements'));
    }

    public function recovery($announcement_id)
    {
        return $this->setAccepted($announcement_id, true);
      
    }
    public function delete(Announcement $announcement)
    {
        
        $announcement->delete();
        return redirect(route('revisor.trash'))->with('message' , 'hai cancellato il messaggio');
      
    }
}


// is_accepted = true ( accettato ), false ( da revisionare ), null ( cestino )
// if($announcement->is_accepted == null)