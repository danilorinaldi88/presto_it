<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $user = Auth::user();
        return view('profile.create', compact('user'));
    }


    public function edit()
    {   $user = Auth::user();
        return view('profile.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $user = Auth::user();
       
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        return view('profile.create', compact('user'));
    }
    
        public function delete(User $user){
         $user->delete();
         $users=User::all();
         return view('roles.admin', compact('users'));
    }
    
}
