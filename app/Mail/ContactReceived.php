<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactReceived extends Mailable
{

   
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $contact;

    public function __construct($contatto)
    {
    $this->contact = $contatto;
    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        {
            return $this->from('lorenzo.gian@gmail.com')
                        ->view('mail/contact_received');
                        
            }
    }
}
